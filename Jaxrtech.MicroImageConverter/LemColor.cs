﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jaxrtech.MicroImageConverter
{
    class LemColor
    {
        public const byte MaxValue = 15;

        private byte _R;
        public byte R
        {
            get { return _R; }
            set
            {
                CheckValue(value);
                _R = value;
            }
        }

        private byte _G;
        public byte G
        {
            get { return _G; }
            set
            {
                CheckValue(value);
                _G = value;
            }
        }

        private byte _B;
        public byte B
        {
            get { return _B; }
            set
            {
                CheckValue(value);
                _B = value;
            }
        }

        public LemColor()
        {
            this.R = 0;
            this.G = 0;
            this.B = 0;
        }

        public LemColor(Color color)
        {
            this.R = ConvertValue(color.R);
            this.G = ConvertValue(color.G);
            this.B = ConvertValue(color.B);
        }

        private byte ConvertValue(byte value)
        {
            return (byte)((value / 255.0) * 15);
        }

        public static IEnumerable<LemColor> ConvertPalette(Bitmap bitmap)
        {
            foreach (var color in bitmap.Palette.Entries)
            {
                yield return new LemColor(color);
            }
        }

        private void CheckValue(byte value)
        {
            if (value > MaxValue) throw new ArgumentOutOfRangeException("Value ranges from 0-15");
        }

        public ushort ToWord()
        {
            ushort data = 0;

            // The data format is '0000rrrrggggbbbb' (in LSB-0)
            data = (ushort)((R << 8) | (G << 4) | B);

            return data;
        }
    }
}
