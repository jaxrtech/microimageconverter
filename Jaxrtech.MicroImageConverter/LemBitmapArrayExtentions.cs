﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jaxrtech.MicroImageConverter
{
    static class LemBitmapArrayExtentions
    {
        public static ushort[] ToFontWords(this LemBitmap[] array)
        {
            ushort[] fonts = new ushort[array.Length * 2];
            for (int i = 0; i < array.Length; i++)
            {
                ushort[] words = array[i].ToFontWords();
                Array.Copy(words, 0, fonts, i * 2, words.Length);
            }

            return fonts.ToArray();
        }
    }
}
