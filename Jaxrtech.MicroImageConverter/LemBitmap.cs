﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Jaxrtech.MicroImageConverter
{
    class LemBitmap : IEquatable<LemBitmap>
    {
        public int Width { get; private set; }
        public int Height { get; private set; }
        public LemColor[] Palette { get; private set; }

        byte[,] data;

        public LemBitmap(int width, int height)
        {
            this.Width = width;
            this.Height = height;

            data = new byte[Width, Height];
        }

        public LemBitmap(Bitmap bitmap)
        {
            if (bitmap.PixelFormat != PixelFormat.Format8bppIndexed)
            {
                throw new FormatException("Bitmap provided is not in the indexed 8bpp format");
            }

            Width = bitmap.Width;
            Height = bitmap.Height;
            Palette = LemColor.ConvertPalette(bitmap).ToArray();

            data = new byte[Width, Height];
            byte[] raw = bitmap.ToBytes();

            // Copy image rows into data
            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    data[x, y] = raw[(y * Width) + x];
                }
            }
        }

        public void SetPixelAt(int x, int y, byte bit)
        {
            bit = (byte)(bit & 0x01);
            data[x, y] = bit;
        }

        public byte GetPixelAt(int x, int y)
        {
            return data[x, y];
        }

        public void Visualize()
        {
            Console.WriteLine("----");
            for (int y = 0; y < Height; y++)
            {
                string line = "";
                for (int x = 0; x < Width; x++)
                {
                    byte value = data[x, y];
                    char c = (value == 0) ? ' ' : '\u2588';
                    line += c;
                }
                Console.WriteLine(line);
            }
            Console.WriteLine("----");
        }

        public ushort[] ToFontWords()
        {
            /* Each font is formatted to have each column as a consecutive 
             * byte in each word
             * 
             * From LEM1802 documentation:
             * For example, the character F looks like this:
             *    word0 = 1111111100001001
             *    word1 = 0000100100000000
             * Or, split into octets:
             *    word0 = 11111111 /
             *            00001001
             *    word1 = 00001001 /
             *            00000000
             */
            CheckSizeForFont();

            ushort[] mem = new ushort[Globals.FontSizeInWords];

            // Loop through the X first since the font is basiclly sideways
            int x = 0;
            while (x < Width)
            {
                // Get current column bytes
                byte[] columns = new byte[2];
                for (int i = 0; i < 2; i++)
                {
                    for (int y = 0; y < Height; y++)
                    {
                        columns[i] = (byte)(columns[i] | (data[x, y] & 0x01) << y);
                    }
                    // Only increment once
                    if (i == 0) x++;
                }

                mem[x / 2] = (ushort)((columns[0] << 8) | columns[1]);
                x++;
            }

            return mem;
        }

        private void CheckSizeForFont()
        {
            if (Width != Globals.CellWidth || Height != Globals.CellHeight)
            {
                throw new IndexOutOfRangeException("Bitmap cannot be used for a font as the height and width are incorrect.");
            }
        }

        public int NumberOfBitsDiffrentFrom(LemBitmap other)
        {
            int sum = 0;

            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    if (GetPixelAt(x, y) != other.GetPixelAt(x, y))
                        sum++;
                }
            }

            return sum;
        }

        public override int GetHashCode()
        {
            int sum = 17;

            unchecked
            {
                for (int y = 0; y < Height; y++)
                {
                    for (int x = 0; x < Width; x++)
                    {
                        sum = sum * 23 + data[x, y].GetHashCode();
                    }
                }
            }

            return sum;
        }

        public bool Equals(LemBitmap other)
        {
            if (this.Width != other.Width || this.Height != other.Height)
            {
                return false;
            }

            for (int y = 0; y < this.Height; y++)
            {
                for (int x = 0; x < this.Width; x++)
                {
                    if (GetPixelAt(x, y) != other.GetPixelAt(x, y))
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}
