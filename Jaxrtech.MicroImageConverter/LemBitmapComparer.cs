﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jaxrtech.MicroImageConverter
{
    class LemBitmapComparer : IEqualityComparer<LemBitmap>
    {
        public bool Equals(LemBitmap x, LemBitmap y)
        {
            return x.Equals(y);
        }

        public int GetHashCode(LemBitmap obj)
        {
            return obj.GetHashCode();
        }
    }
}
