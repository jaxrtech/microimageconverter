﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jaxrtech.MicroImageConverter
{
    static class Globals
    {
        public const int MaxSourceColors = 2;
        public const int MaxFonts = 128;
        public const int MaxPaletteColors = 16;

        public const int FontTableSizeInWords = MaxFonts * 2;
        public const int CellTableSizeInWords = TotalCells;
        public const int ColorTableSizeInWords = MaxPaletteColors;

        public const int ScreenWidth = 128;
        public const int ScreenHeight = 96;
        
        public const int TotalCells = CellColumns * CellRows;
        public const int CellColumns = 32;
        public const int CellRows = 12;
        
        public const int CellWidth = 4;
        public const int CellHeight = 8;

        public const int FontSizeInWords = 2;
    }
}
