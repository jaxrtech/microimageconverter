﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jaxrtech.MicroImageConverter
{
    /// <summary>
    /// Specifies how the inital image should be dealt with.
    /// </summary>
    enum ConversionMode
    {
        /// <summary>
        /// Expects the orginial image to be already properly formatted
        /// </summary>
        None,

        /// <summary>
        /// Sizes the image down to the LEM display and applies the necessary color limitations
        /// </summary>
        SizeAndColor
    }
}
