﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jaxrtech.MicroImageConverter
{
    static class UshortArrayExtentions
    {
        private const int HexValuesPerLine = 5;

        public static string ToHexWordString(this ushort[] values)
        {
            StringBuilder s = new StringBuilder();
            bool first = true;
            int i = 0;
            foreach (var hex in ToHexWords(values))
            {
                // Go to next line every so many words
                if (i % HexValuesPerLine == 0 || i == 0)
                {
                    if (i != 0)
                        s.AppendLine();
                    s.Append("dat ");
                    first = true;
                }

                if (first)
                {
                    s.Append(hex);
                    first = false;
                }
                else
                {
                    s.Append(", " + hex);
                }

                i++;
            }

            return s.ToString();
        }

        public static IEnumerable<string> ToHexWords(this ushort[] values)
        {
            string rawHex = "";
            foreach (var v in values)
            {
                rawHex += v.ToString("x4");
            }

            foreach (var value in rawHex.SplitInParts(4))
            {
                yield return "0x" + value;
            }
        }
    }
}
