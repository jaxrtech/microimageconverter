﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jaxrtech.MicroImageConverter
{
    static class CodeGenerator
    {
        private const string videoRamName = "video_ram";
        private const string paletteTableName = "palette_table";
        private const string fontTableName = "font_table";

        private const string initizationAsm = @"
hwn i
:loop
    sub i, 0x1
    hwq i
    set z, 0x01
    ifn a, 0xf615
        set z, 0x00
    ifn b, 0x7349
        set z, 0x00
    ife z, 0x00
        set pc, loop

set a, 0x0
set b, " + videoRamName + @"
hwi i

set a, 0x1
set b, " + fontTableName + @"
hwi i

set a, 0x2
set b, " + paletteTableName + @"
hwi i

:halt 
    set pc, halt";

        public static void GenerateCode(string codePath, LemCharacter[] characters, LemBitmap[] fonts, LemColor[] palette)
        {
            using (StreamWriter codeFile = new StreamWriter(File.Open(codePath, FileMode.Create, FileAccess.ReadWrite)))
            {
                codeFile.WriteLine(initizationAsm);
                codeFile.WriteLine();
                WriteCellTable(characters, codeFile);
                codeFile.WriteLine();
                WriteFontTable(fonts, codeFile);
                codeFile.WriteLine();
                WritePalleteTable(palette, codeFile);
            }
        }

        private static void WriteCellTable(LemCharacter[] indexedCells, TextWriter writer)
        {
            ushort[] video = indexedCells.ToVideoWords();
            writer.WriteLine(":" + videoRamName);
            writer.WriteLine(video.ToHexWordString());
        }

        private static void WritePalleteTable(LemColor[] colors, TextWriter writer)
        {
            ushort[] palette = colors.ToPaletteWords();
            writer.WriteLine(":" + paletteTableName);
            writer.WriteLine(palette.ToHexWordString());
        }

        private static void WriteFontTable(LemBitmap[] uniqueFonts, TextWriter writer)
        {
            ushort[] fonts = uniqueFonts.ToFontWords();
            writer.WriteLine(":" + fontTableName);
            writer.WriteLine(fonts.ToHexWordString());
        }
    }
}
