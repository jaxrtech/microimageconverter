﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Jaxrtech.MicroImageConverter
{
    static class BitmapExtentions
    {
        public static byte[] ToBytes(this Bitmap bitmap)
        {
            BitmapData data = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width - 1, bitmap.Height - 1), ImageLockMode.ReadOnly, bitmap.PixelFormat);
            int size = bitmap.Height * data.Stride; // not sure why but data.Height is 1 too low for some reason
            byte[] bytes = new byte[size];
            Marshal.Copy(data.Scan0, bytes, 0, size);
            bitmap.UnlockBits(data);

            return bytes;
        }
    }
}
