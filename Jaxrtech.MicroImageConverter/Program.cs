﻿using AForge.Imaging.ColorReduction;
using AForge.Imaging.Filters;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Jaxrtech.MicroImageConverter
{
    class Program
    {
        private static Bitmap image;
        private static ConversionMode mode;
        private static string imagePath;
        private static string codePath;

        static void Main(string[] args)
        {
            LoadArguments(args);

            // Load file
            image = LoadImage(imagePath);
            if (image == null) return;

            // Check the image
            bool verified = CheckImage(image);
            if (!verified && mode == ConversionMode.SizeAndColor)
            {
                if (!CheckSize(image))
                {
                    // Resize the image to the screen
                    ResizeBicubic filter = new ResizeBicubic(Globals.ScreenWidth, Globals.ScreenHeight);
                    image = filter.Apply(image);
                }

                if (!CheckPaletteSize(image))
                {
                    // Limit the number of colors on the image
                    ColorImageQuantizer ciq = new ColorImageQuantizer(new MedianCutQuantizer());
                    image = ciq.ReduceColors(image, Globals.MaxPaletteColors);
                }
            }

            // Chop the image into the cells
            LemBitmap[] cells = DivideIntoCells();
            LemBitmap[] fonts = GetUniqueCells(cells);
            if (fonts.Length > Globals.MaxFonts)
            {
                var values = MergeSimilarCellsToLimit(cells, fonts); // TODO: Finish the code to merge cells
                cells = values.Item1; // TODO: might want to make with a struct or something later
                fonts = values.Item2;
            }
            Console.WriteLine("Success: Squashed to {0} (max 128) of the displays total {1} cells", fonts.Length, Globals.TotalCells);

            LemCharacter[] characters = IndexCells(cells, fonts);

            // Open file to save code
            CodeGenerator.GenerateCode(codePath, characters, fonts, fonts[0].Palette);

            // Visualise entire image
            LemBitmap indexed = new LemBitmap(image);
            indexed.Visualize();
        }

        private static LemCharacter[] IndexCells(LemBitmap[] cells, LemBitmap[] fonts)
        {
            LemCharacter[] indexedBlocks = new LemCharacter[Globals.TotalCells];
            for (int i = 0; i < Globals.TotalCells; i++)
            {
                byte index = (byte)(Array.IndexOf<LemBitmap>(fonts, cells[i]));
                if (index == 255) throw new InvalidOperationException("Index should never be -1");
                byte background = 0;
                byte foreground = 1;
                LemCharacter block = new LemCharacter(index, foreground, background, false);
                indexedBlocks[i] = block;
            }

            return indexedBlocks;
        }

        private static Tuple<LemBitmap[], LemBitmap[]> MergeSimilarCellsToLimit(LemBitmap[] cells, LemBitmap[] fonts)
        {
            int[][] scores = new int[fonts.Length][];
            for (int source = 0; source < fonts.Length; source++)
            {
                // Get a score for each other block except itselft
                scores[source] = new int[fonts.Length];
                for (int comparison = 0; comparison < fonts.Length; comparison++)
                {
                    // Ensure a skip for itself
                    if (source == comparison) scores[source][comparison] = int.MaxValue;
                    scores[source][comparison] = fonts[source].NumberOfBitsDiffrentFrom(fonts[comparison]);
                }
            }

            // TODO: Probably want to change the font with the lowest number first but
            //       I really just want to see this work
            List<int> badFonts = new List<int>(); // fonts that are getting marged
            Dictionary<int, int> merges = new Dictionary<int, int>();
            for (int i = 0; fonts.Length - merges.Count > Globals.MaxFonts; i++)
            {
                bool isGood = false;
                int newIndex = 0;
                while (!isGood)
                {
                    newIndex = Array.IndexOf(scores[i], scores[i].Min());
                    if (newIndex == int.MaxValue)
                        throw new InvalidOperationException("Index value reached maximum.");
                    if (badFonts.Contains(newIndex) || newIndex == i)
                    {
                        scores[i][newIndex] = int.MaxValue;
                        continue;
                    }
                    else isGood = true;
                }

                merges.Add(i, newIndex);

                // Update the previously changed fonts to the new one
                for (int prev = 0; prev < i; prev++)
                {
                    if (merges[prev] == i) merges[prev] = newIndex;
                }

                badFonts.Add(i);
            }

            // Copy over the similar font to do another search
            foreach (var merge in merges)
            {
                // TODO: This for some reason is getting really messed up
                //       All this needs to do is update the cell and font with the new merge
                int index = Array.IndexOf(cells, fonts[merge.Key]);
                if (index < 0) throw new InvalidOperationException("Index should never be -1.");
                cells[index] = fonts[merge.Value];
                fonts[merge.Key] = fonts[merge.Value];
            }

            return new Tuple<LemBitmap[], LemBitmap[]>(cells, GetUniqueCells(fonts));
        }

        private static LemBitmap[] GetUniqueCells(LemBitmap[] blocks)
        {
            LemBitmap[] uniqueValues = blocks.Distinct(new LemBitmapComparer()).ToArray();
            return uniqueValues;
        }

        private static LemBitmap[] DivideIntoCells()
        {
            LemBitmap[] blocks = new LemBitmap[Globals.TotalCells];
            for (int y = 0; y < Globals.CellRows; y++)
            {
                for (int x = 0; x < Globals.CellColumns; x++)
                {
                    Rectangle rect = new Rectangle(x * Globals.CellWidth, y * Globals.CellHeight, Globals.CellWidth, Globals.CellHeight);
                    PixelFormat format = image.PixelFormat;
                    Bitmap cell = image.Clone(rect, format);
                    LemBitmap indexed = new LemBitmap(cell);

                    blocks[(Globals.CellColumns * y) + x] = indexed;
                }
            }
            return blocks;
        }

        private static Bitmap LoadImage(string file)
        {
            Bitmap image;
            try
            {
                image = new Bitmap(file);
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Error: File not found");
                return null;
            }
            return image;
        }

        private static bool CheckImage(Image image)
        {
            bool success = true;
            if (!CheckSize(image)) success = false;
            if (!CheckPaletteSize(image)) success = false;

            return success;
        }

        private static bool CheckPaletteSize(Image image)
        {
            int colors = image.Palette.Entries.Length;
            if (colors > Globals.MaxSourceColors)
            {
                if (mode == ConversionMode.None)
                    Console.WriteLine("Error: Image palette is greater than {0} colors. Got a palette of {1} colors.", Globals.MaxSourceColors, colors);
                return false;
            }
            else
            {
                return true;
            }
        }

        private static bool CheckSize(Image image)
        {
            if (image.Width != Globals.ScreenWidth && image.Height != Globals.ScreenHeight)
            {
                if (mode == ConversionMode.None)
                    Console.WriteLine("Error: Image size is invalid. Expected a {0}x{1} image.", Globals.ScreenHeight, Globals.ScreenHeight);
                return false;
            }
            else
            {
                return true;
            }
        }

        private static void LoadArguments(string[] args)
        {
            if (args.Length > 3)
            {
                PrintHelp();
                return;
            }

            // Save paths
            int pathsIndex = 0;
            if (args[0] == "-strict")
            {
                mode = ConversionMode.None;
                pathsIndex++;
            }
            else
            {
                mode = ConversionMode.SizeAndColor;
            }

            imagePath = args[pathsIndex];
            codePath = args[pathsIndex + 1];
        }

        private static void PrintHelp()
        {
            Console.WriteLine("usage: microimg [-strict] [image_path] [code_path]");
            Console.WriteLine();
        }
    }
}
