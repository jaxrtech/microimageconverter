﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jaxrtech.MicroImageConverter
{
    class LemCharacter
    {
        private byte _CharacterIndex;
        public byte CharacterIndex
        {
            get { return _CharacterIndex; }
            set
            {
                CheckFontIndex(value);
                _CharacterIndex = value;
            }
        }


        private byte _ForegroundIndex;
        public byte ForegroundIndex
        {
            get { return _ForegroundIndex; }
            set
            {
                CheckColorIndex(value);
                _ForegroundIndex = value;
            }
        }

        private byte _BackgroundIndex;
        public byte BackgroundIndex
        {
            get { return _BackgroundIndex; }
            set
            {
                CheckColorIndex(value);
                _BackgroundIndex = value;
            }
        }

        public bool IsBlinking { get; set; }

        public LemCharacter()
        {
            this.CharacterIndex = 0;
            this.ForegroundIndex = 0;
            this.BackgroundIndex = 0;
            this.IsBlinking = false;
        }

        public LemCharacter(byte character, byte foreground, byte background, bool blinking)
        {
            this.CharacterIndex = character;
            this.ForegroundIndex = foreground;
            this.BackgroundIndex = background;
            this.IsBlinking = blinking;
        }

        public ushort ToWord()
        {
            byte blinking = (IsBlinking) ? (byte)1 : (byte)0;
            return (ushort)(((ForegroundIndex & 0x0f) << 12) | ((BackgroundIndex & 0x0f) << 8) | ((blinking & 0x01) << 7) | (CharacterIndex & 0x7f));
        }

        private static void CheckColorIndex(byte index)
        {
            if (index > Globals.MaxPaletteColors - 1)
            {
                throw new ArgumentOutOfRangeException("index");
            }
        }

        private static void CheckFontIndex(byte index)
        {
            if (index > Globals.MaxFonts - 1)
            {
                 throw new ArgumentOutOfRangeException("index");
            }
        }
    }
}
