﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jaxrtech.MicroImageConverter
{
    static class LemCharacterArrayExtentions
    {
        public static ushort[] ToVideoWords(this LemCharacter[] array)
        {
            ushort[] fonts = new ushort[Globals.TotalCells];
            for (int i = 0; i < array.Length; i++)
            {
                fonts[i] = array[i].ToWord();
            }

            return fonts;
        }
    }
}
