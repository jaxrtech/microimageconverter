﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jaxrtech.MicroImageConverter
{
    static class LemColorArrayExtentions
    {
        public static ushort[] ToPaletteWords(this LemColor[] colors)
        {
            ushort[] data = new ushort[Globals.MaxPaletteColors];
            for (int i = 0; i < colors.Length; i++)
            {
                data[i] = colors[i].ToWord();
            }

            return data;
        }
    }
}
